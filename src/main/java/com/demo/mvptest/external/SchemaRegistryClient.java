package com.demo.mvptest.external;

import com.demo.mvptest.Decision;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@AllArgsConstructor
public class SchemaRegistryClient {

    private static final Logger logger = LogManager.getLogger(SchemaRegistryClient.class);
    private static final String schemaUrl =
            "http://ircc-apicurioregistry.openshift-operators.apps.opc-hip-apps.aws.ci.gc.ca/api/artifacts/decision-value/test";

    private final RestTemplate restTemplate;

    public boolean isArtifactSchemaValid(Decision decision) {
        HttpHeaders headers = buildHttpHeaders();
        HttpEntity<String> httpEntity = new HttpEntity<>(decision.getSchema().toString(), headers);
        logger.info("Validating decision schema against schema registry");
        ResponseEntity<String> response = restTemplate.exchange(schemaUrl, HttpMethod.PUT, httpEntity, String.class);
        return response.getStatusCode().is2xxSuccessful();
    }

    private HttpHeaders buildHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
        headers.add("Accept", "*/*");
        return headers;
    }
}
