package com.demo.mvptest.routes;

import com.demo.mvptest.Decision;
import com.demo.mvptest.entity.TransactionEntity;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class MongoDbRoutes extends RouteBuilder {

        @Override
        public void configure() {

            from("direct:saveTransaction")
                    .bean(this, "adaptToEntity")
                    .log("HIP: Saving Transactional Data to MongoDB")
                    .to("mongodb:mongoClient?database=demodb&collection=test&operation=insert");
        }

    public void adaptToEntity(Exchange exchange) {
        Message in = exchange.getIn();
        Decision decision = in.getBody(Decision.class);
        in.setBody(new TransactionEntity(in.getHeader("kafka.KEY", String.class),
                   decision.getApplicationNumber(),
                   decision.getStatus()));
    }
}
