package com.demo.mvptest.routes;

import com.demo.mvptest.Decision;
import com.demo.mvptest.external.SchemaRegistryClient;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Message;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

@Component
public class KafkaRoutes extends RouteBuilder {

    private static final Logger logger = LogManager.getLogger(JMSRoutes.class);

    @Override
    public void configure() {

        from("direct:publish-to-hip")
                .log("CPT: Sending decision payload to HIP")
                .to("kafka:decision-topic?brokers=172.30.254.39:9092")
                .marshal().avro("com.demo.mvptest.Decision")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(202))
                .setBody(constant("Decision sent to GCMS!"));

        from("kafka:decision-topic?brokers=172.30.254.39:9092")
                .setHeader(KafkaConstants.KEY, simple("${random(100000, 5000000)}", String.class))
                .log("HIP: Received decision from CPT ${body}")
                .unmarshal().avro("com.demo.mvptest.Decision")
                .multicast()
                .to("direct:saveTransaction")
                .to("direct:publish-to-mq-sink");

/*        from("direct:publish-to-mq-sink")
                .log("HIP: Sending Decision to Kafka with CorrelationID: ${headers[kafka.KEY]}")
                .onException(Exception.class)
                    .maximumRedeliveries(2)
                    .redeliveryDelay(1000)
                    .logRetryAttempted(true)
                    .retryAttemptedLogLevel(LoggingLevel.WARN)
                    .handled(true)
                    .log("Unable to deliver message after 3 attempts! Sending message to dead letter queue")
                    .end()
                .to("kafka:mvp-sink-topic?brokers=172.30.254.39:9092&serializerClass="+CustomKafkaAvroSerializer.class.getName());

        from("kafka:mvp-source-decision-topic?brokers=172.30.254.39:9092")
                .log("HIP: Received GCMS Confirmation from kafka with CorrelationID: ${headers[kafka.KEY]}")
                .unmarshal().avro("com.demo.mvptest.Decision")
                .log("HIP: Sending confirmation payload back to CPT");*/
    }

/*    public void validateSchema(Exchange exchange) {
        Message in = exchange.getIn();
        Decision decision = in.getBody(Decision.class);
        if (!schemaRegistryClient.isArtifactSchemaValid(decision)) {
            throw new IllegalArgumentException("Decision Schema is not compatible with schema found on the Schema Registry");
        }
        else {
            logger.info("Schema validated!");
        }
    }*/
}
