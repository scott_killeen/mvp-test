package com.demo.mvptest.routes;

import com.demo.mvptest.Decision;
import io.confluent.kafka.schemaregistry.avro.AvroSchema;
import io.confluent.kafka.schemaregistry.client.CachedSchemaRegistryClient;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerializer;
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Collections;
import java.util.List;
import java.util.Map;


public class CustomKafkaAvroSerializer extends AbstractKafkaAvroSerializer implements Serializer<Object> {

    private static final String SCHEMA_REGISTRY_URL = "http://ircc-apicurioregistry.openshift-operators.apps.opc-hip-apps.aws.ci.gc.ca/apis/ccompat/v6";

    @Override
    public void configure(KafkaAvroSerializerConfig config) {
        try {
            final List<String> schemas =
                    Collections.singletonList(SCHEMA_REGISTRY_URL);
            this.schemaRegistry = new CachedSchemaRegistryClient(schemas,
                    Integer.MAX_VALUE);
            this.autoRegisterSchema = true;
        } catch (ConfigException e) {
            throw new org.apache.kafka.common.config.ConfigException(e.getMessage());
        }
    }

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
        configure(null);
    }

    @Override
    public byte[] serialize(String subject, Object schema) {
        AvroSchema avroSchema = new AvroSchema(Decision.getClassSchema());
        subject = subject +"-value";
        return serializeImpl(subject, schema, avroSchema);
    }

    @Override
    public void close() {
    }
}
