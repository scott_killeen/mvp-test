package com.demo.mvptest.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;


@Component
public class JMSRoutes extends RouteBuilder {

    @Override
    public void configure() {

        from("direct:write-to-mq")
                .log("HIP: Sending Decision to Kafka with CorrelationID: ${headers[kafka.KEY]}")
                .to("jms:queue:HIPMVPGCMS?requestTimeout=4500");

        from("jms:queue:HIPMVPGCMS")
                .log("GCMS: Received decision with CorrelationID: ${headers.JMSCorrelationID} and body: ${body}.")
                .log("GCMS: Processing decision...")
                .log("GCMS: Success! Sending confirmation to response queue")
                .to("jms:queue:RESPONSEQUEUE?requestTimeout=4500");

        from("jms:queue:RESPONSEQUEUE")
                .log("HIP: Received Decision Confirmation from GCMS")
                .log("HIP: Sending confirmation payload back to CPT");

    }

}

