package com.demo.mvptest.routes;

import com.demo.mvptest.Decision;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

@Component
public class RestRoutes extends RouteBuilder {

    @Override
    public void configure() {
        restConfiguration().component("servlet").bindingMode(RestBindingMode.json);

        rest().get("/hello-world").produces(MediaType.APPLICATION_JSON_VALUE)
                .route().setBody(constant("Hello World From the MVP Service 2.0!"));

        rest().post("publish-decision")
                .type(Decision.class)
                .to("direct:publish-to-hip");
    }


}
