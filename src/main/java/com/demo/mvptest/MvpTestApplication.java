package com.demo.mvptest;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MvpTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvpTestApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public MongoClient mongoClient() {
		return new MongoClient(new MongoClientURI("mongodb://admin:password@172.30.211.250:27017/"));
	}
}
