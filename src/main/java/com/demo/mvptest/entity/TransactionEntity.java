package com.demo.mvptest.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEntity {

    private String correlationId;
    private String applicationNumber;
    private String status;

}
